/* postリクエスト時にjson形式のリクエストをquery形式へ変換 */

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
  // Now safe to use the Cordova API
}
// This is a JavaScript file
var app = angular.module('slolab', ['ngRoute', 'ngAnimate', 'ngResource'])

  .config(function ($httpProvider, $locationProvider) {
    // $locationProvider.html5Mode(true).hashPrefix('#');
    // 通信時のコンテントタイプ指定
    $httpProvider.defaults.headers.common = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }; 
    $httpProvider.defaults.headers.post = { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' }; 
    // json から params フォーマットへ変換
    $httpProvider.defaults.transformRequest = function(data){
      if (data === undefined) {
        return data;
      }
      if (localStorage.getItem('remember_token')) {
        data['remember_token'] = localStorage.getItem('remember_token');
      }
      return $.param(data);
    }
  })
  .value();

  app.config(function ($httpProvider) {
    $httpProvider.defaults.transformRequest = function(data){
      if (data === undefined) {
        return data;
      }
      return $.param(data);
    }
  });

/********************** pageCtrl **********************/
app.controller("pageCtrl", function($scope, $route, $location, sessionService, urlService) {
  // Hashbang 対応
  $scope.$route         = $route;
  $scope.$location      = $location;
  $scope.sessionService = sessionService;
  $scope.urlService    = urlService;

  // 起動時ログイン確認
  if (sessionService.getRememberToken()) {
    if (!$location.path()) {
      $location.path('/home');
    }
  }else{
    $location.path('/login');
  }

  $scope.isAnimate = false;
});

/********************** sessionCtrl **********************/
app.controller("sessionsCtrl", function($scope, $resource, $location) {
  // 開発・本番環境毎の定数定義
  /* if (monaca.isAndroid === true || monaca.isIOS === true) {
     $scope.URL = "http://slo-lab.net/";
     }else{
     $scope.URL = "http://another.media-shelf.net:3005";
     } */
  user = $resource(url + 'sessions', {});

  $scope.login = function(email, password) {
    
    user.save({ session: { "email" : email, "password" : password } },
              function success(data) {
                localStorage.setItem('remember_token', data.user.remember_token);
                $location.path('/home');
              },
              function error (data, status) {
                user.message = data['error'];
              });
  }
  $scope.logout = function(){
    localStorage.removeItem('remember_token');
    $location.path('/login');
  }
});

