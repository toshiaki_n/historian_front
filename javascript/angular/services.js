app
.service('urlService', function() {
  url    = "http://api.media-shelf.net:3005/v1/";

  this.pathHome = function() {
    return 'home';
  }
  this.pathHome = function() {
    return 'list_all';
  }
  this.pathUsers = function() {
    return (arguments.length == 1) ? ('users/' + arguments[0] + '/') : ('users/')
  }
  this.pathLogs = function() {
    return (arguments.length == 1) ? ('logs/'  + arguments[0] + '/') : ('logs/')
  }
  this.pathLogDetails = function() {
    return (arguments.length == 1) ?
      ('logs/' + arguments[0] + '/log_details/') :
      ('logs/' + arguments[0] + '/log_details/' + arguments[1] + '/')
  }
  this.pathComments = function() {
    return (arguments.length == 1) ? ('comments/' + arguments[0] + '/') : ('comments/')
  }

  this.urlUsers = function() {
    return (arguments.length == 1) ? (url + 'users/' + arguments[0] + '/') : (url + 'users/')
  }
  this.urlLogs = function() {
    return (arguments.length == 1) ? (url + 'logs/'  + arguments[0] + '/') : (url + 'logs/')
  }
  this.urlMachines = function() {
    return url + 'machines/'
  }
  this.urlLogDetails = function() {
    return (arguments.length == 1) ? (url + 'log_details/' + arguments[0] + '/') : (url + 'log_details/')
  }
  this.urlComments = function() {
    return (arguments.length == 1) ? (url + 'comments/'    + arguments[0] + '/') : (url + 'comments/')
  }
})
.service('sessionService', function() {
  this.isCurrentUser =  function(remember_token) {
    return (remember_token == this.getRememberToken());
  }
  this.getRememberToken = function() {
    return localStorage.getItem('remember_token');
  }
  this.setRememberToken = function(remember_token) {
    return localStorage.setItem('remember_token', remember_token);
  }
});
