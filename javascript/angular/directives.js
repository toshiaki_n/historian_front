app
/* --------------- 共通 --------------- */
.directive('slHeader', function() {
  console.log(template = 'views/shared/header.html');
  return {
    restrict: "E",
    templateUrl: template
  };
})

.directive('slContainer', function() {
  console.log(template = 'views/shared/container.html');
  return {
    restrict: "E",
    templateUrl: template
  };
})    
.directive('slTabbar', function() {
  console.log(template = 'views/shared/tabbar.html');
  return {
    restrict: "E",
    templateUrl: template
  };
})
.directive('slError', function() {
  return {
    restrict: "E",
    templateUrl: 'views/shared/_error.html'
  };
})
.directive('ngAutoComplete', function($timeout, $resource, urlService, sessionService) {
  return function(scope, iElement, iAttrs) {
    params_remember_token = { remember_token: sessionService.getRememberToken() };
    Machines = $resource(urlService.urlMachines(), params_remember_token);
    Machines.query({}, true,
               function success(data) { machines = data; },
               function error(data, status) {
                 // localStorage.removeItem('remember_token');
                 // $scope.template = { name: 'login', url: '/login.html' };
               });
    iElement.autocomplete({
      source: function (request, response) {
        response(
          $.map(machines, function(n) {
            if ((n.name + n.kana).indexOf(request.term) != -1) {
              return n.name
            }else{
              return null;
            }
          })
        )
      },
      select: function() {
        $timeout(function() {
          iElement.trigger('input');
        }, 0);
      },
      delay: 0,
      minLength: 2
    });
  };
})
.directive('slFormImages', function() {
  return {
    restrict: "E",
    templateUrl: 'views/shared/_form_images.html'
  };
})
/* --------------- ログ関連 --------------- */
.directive('slLog', function() {
  return {
    restrict: "E",
    templateUrl: 'views/shared/_log.html'
  };
})
.directive('slFormLog', function() {
  return {
    restrict: "E",
    templateUrl: 'views/logs/_form.html'
  };
})
.directive('slComment', function() {
  return {
    restrict: "E",
    templateUrl: 'views/shared/_comment.html'
  };
})
/* --------------- ログ詳細関連 --------------- */
.directive('slFormLogDetail', function() {
  return {
    restrict: "E",
    replace: true,
    templateUrl: 'views/log_details/_form.html'
  };
})
.directive('slLogDetailCol', function() {
  return {
    restrict: "A", // 中身がtrタグなため利用時は属性指定で使われる
    templateUrl: 'views/shared/_log_detail_col.html'
  };
})
.directive('slLogDetail', function() {
  console.log(template = 'views/shared/_log_detail.html');
  return {
    restrict: "E",
    templateUrl: template,
  };
})

.directive("slAddImage", function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {

      element.on('change', function() {

        file  = element.prop('files')[0];

        console.log('here2');

        // File APIを使用し、ローカルファイルを読み込む
        reader = new FileReader();

        // 読み込みが完了したら、画像を表示させる

        reader.onload = function(e) {
          element.parent().prepend('<img src="' + e.target.result + '" class="thumbnail" style="width: 200px;">');
          element.parent().append('<span class="btn btn-default btn-small">削除</span>');
          console.log(scope.images);
        };

        // 画像の読み込みを開始
        reader.readAsDataURL(file);
      });
    }
  }
})
app.directive('slFileUpload', function () {
  return {
    scope: true,        //create a new scope
    link: function (scope, element, attrs) {
      element.bind('change', function (event) {
        file = event.target.files[0];

        reader = new FileReader();

        // 読み込みが完了したら、画像を表示させる
        reader.onload = function(e) {
          console.log(e.target.result);
          file.src = e.target.result;
          //emit event upward
          scope.$emit("fileSelected", { file: file });
        };

        // 画像の読み込みを開始
        reader.readAsDataURL(file);
      });
    }
  };
});
