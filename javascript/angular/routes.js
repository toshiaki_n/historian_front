app.config(function($routeProvider, $locationProvider) {
  // $locationProvider.html5Mode(true);
  // prefix = '';

  $routeProvider.when('/home',
                      { templateUrl: 'views/log_details/list_followed.html', controller: 'logDetailsCtrl' });
  $routeProvider.when('/list_all',
                      { templateUrl: 'views/log_details/list_all.html',      controller: 'logDetailsCtrl' });
  $routeProvider.when('/login',
                      { templateUrl: 'views/sessions/login.html',            controller: 'sessionsCtrl' });
  $routeProvider.when('/logs/new',
                      { templateUrl: 'views/logs/new.html',                  controller: 'logsCtrl' });
  $routeProvider.when('/logs/list_all',
                      { templateUrl: 'views/logs/list_all.html',             controller: 'logsCtrl' });
  $routeProvider.when('/logs/:id/',
                      { templateUrl: 'views/logs/show.html',                 controller: 'logsCtrl' });
  $routeProvider.when('/logs/:l_id/log_details/new',
                      { templateUrl: 'views/log_details/new.html',           controller: 'logDetailsCtrl' });
  $routeProvider.when('/logs/:l_id/log_details/:ld_id/edit',                 
                      { templateUrl: 'views/log_details/edit.html',          controller: 'logDetailsCtrl' });
  $routeProvider.when('/logs/:l_id/log_details/:ld_id/',                     
                      { templateUrl: 'views/log_details/show.html',          controller: 'logDetailsCtrl' });
});
