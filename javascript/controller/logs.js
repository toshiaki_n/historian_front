app.controller("logsCtrl", function($scope, $resource, $location, sessionService, urlService){
  params_remember_token = { remember_token: sessionService.getRememberToken() };

  Log      = $resource(urlService.urlLogs()  + ':l_id/', params_remember_token);
  Logs     = $resource(urlService.urlLogs(),             params_remember_token);
  User     = $resource(urlService.urlUsers() + '0/',     params_remember_token); // current_user
  Machines = $resource(urlService.urlMachines(),         params_remember_token);
  Comment  = $resource(urlService.urlComments(),         params_remember_token);

  $scope.log            = {};
  $scope.machine        = {};
  $scope.mode           = 'easy';
  $scope.new_comment    = {};
  $scope.show_detail    = {};
  $scope.show_comments  = {};
  $scope.sessionService = sessionService;

  $scope.fetchLogs = function(params){

    if (!params) {
      params = { type: 'followed' };
    }

    Logs.query(params, true,
               function success(data) { $scope.logs = data; },
               function error(data, status) {
                 // localStorage.removeItem('remember_token');
                 // $scope.template = { name: 'login', url: '/login.html' };
               });
  }

  $scope.fetchOneLog = function(params){
    Log.get(params, true,
               function success(data) { $scope.log = data.log; },
               function error(data, status) {
                 //localStorage.removeItem('remember_token');
                 //$scope.template = { name: 'login', url: '/login.html' };
               });
  }
  $scope.fetchCurrentUser = function(params){

    User.get(params, true,
               function success(data) { $scope.current_user = data.user; },
               function error(data, status) {
                 //localStorage.removeItem('remember_token');
                 //$scope.template = { name: 'login', url: '/login.html' };
               });
  }
  $scope.fetchAllMachines = function(params){
    Machines.query({}, true,
               function success(data) { $scope.machines = data; },
               function error(data, status) {
                 // localStorage.removeItem('remember_token');
                 // $scope.template = { name: 'login', url: '/login.html' };
               });
               
  }
  $scope.newLog  = function() {
    $scope.income = ($scope.income == 'minus') ? - $scope.income : $scope.income
    Logs.save(
      { log: $scope.log, income: $scope.income, mode: $scope.mode, machine: $scope.machine },
      function success(data)       { $location.path(urlService.pathLogs(data.log.id)) },
      function error(data, status) { console.log(data) }
    )
  }
  $scope.newCommentForLog  = function(id, content){
    Comment.save( {
      comment: { "resource_type" : "Log", "resource_id" : id, "content" : content } },
      function success(data)       { $scope.log.comments = $scope.log.comments.concat(data.comment) },
      function error(data, status) { console.log(data) }
    )
  }
  $scope.newCommentForLogs = function(id, content, index) {
    Comment.save( {
      comment: { "resource_type" : "Log", "resource_id" : id, "content" : content } },
      function success(data) {
        $scope.logs[index].comments = $scope.logs[index].comments.concat(data.comment);
      },
      function error(data, status) { console.log(data); }
                )
  }
  
  // ログのまとめ表示切替
  $scope.toggleDetail   = function(id) { $scope.show_detail[id] = !$scope.show_detail[id]; }
  // コメント表示切替
  $scope.toggleComments = function(id) { $scope.show_comments[id] = !$scope.show_comments[id]; }
});
