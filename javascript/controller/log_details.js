app.controller("logDetailsCtrl", function($scope, $resource, $http, sessionService, urlService){
  params_remember_token = { remember_token: sessionService.getRememberToken() };

  Log        = $resource(urlService.urlLogs()       + ':l_id/', params_remember_token);
  LogDetail  = $resource(urlService.urlLogDetails() + ':id/',   params_remember_token);
  LogDetails = $resource(urlService.urlLogDetails(),            params_remember_token);

  $scope.log_detail = {};
  $scope.files = [];
  $scope.role = {};

  $scope.$on("fileSelected", function (event, args) {
    $scope.$apply(function () {            
      //add the file object to the scope's files collection
      $scope.files.push(args.file);
    });
  });

  $scope.fetchOneLog = function(params){

    Log.get(params, true,
            function success(data) { $scope.log = data.log; },
            function error(data, status) {
              //localStorage.removeItem('remember_token');
              //$scope.template = { name: 'login', url: '/login.html' };
            });
  }
  $scope.fetchLogDetails = function(params){

    if (!params) {
      params = { type: 'followed' };
    }

    LogDetails.query($.extend(params, params_remember_token), true,
                     function success(data) { $scope.log_details = data; },
                     function error(data, status) {
                       // localStorage.removeItem('remember_token');
                       // $scope.template = { name: 'login', url: '/login.html' };
                     });
  }

  $scope.fetchOneLogDetail = function(params){

    LogDetail.get(params, true,
                  function success(data) {
                    $scope.log_detail = data.log_detail;
                    $scope.images = data.log_detail.images + {};
                  },
                  function error(data, status) {
                    //localStorage.removeItem('remember_token');
                    //$scope.template = { name: 'login', url: '/login.html' };
                  });
  }
  // Todo
  $scope.newLogDetail  = function(){
    $scope.log_detail.income = ($scope.income == 'minus') ? - $scope.income : $scope.income;
    $http({
      method: 'POST',
      url: urlService.urlLogDetails(),
      //IMPORTANT!!! You might think this should be set to 'multipart/form-data' 
      // but this is not true because when we are sending up files the request 
      // needs to include a 'boundary' parameter which identifies the boundary 
      // name between parts in this multi-part request and setting the Content-type 
      // manually will not set this boundary parameter. For whatever reason, 
      // setting the Content-type to 'false' will force the request to automatically
      // populate the headers properly including the boundary parameter.
      headers: { 'Content-Type': undefined },
      //This method will allow us to change how the data is sent up to the server
      // for which we'll need to encapsulate the model data in 'FormData'
      transformRequest: function (data) {
        var formData = new FormData();
        //need to convert our json object to a string version of json otherwise
        // the browser will do a 'toString()' on the object which will result 
        // in the value '[Object object]' on the server.
        formData.append("log_detail", angular.toJson(data.log_detail));
        //now add all of the assigned files

        for (var i = 0; i < data.files.length; i++) {
          console.log('here' + i);
          //add each file to the form data and iteratively name them
          formData.append("files[]", data.files[i]);
        }
        return formData;
      },
      //Create an object that contains the model and files which will be transformed
      // in the above transformRequest method
      data: { log_detail: $scope.log_detail, files: $scope.files }
    }).
      success(function (data, status, headers, config) {
      $location.path(urlService.pathLogDetails(data.log_id, data.id));
    }).
      error(function (data, status, headers, config) {
        console.log(data);
    });
  }
  $scope.newCommentForLogDetail  = function(id, content){
    Comment.save( {
      comment: { "resource_type" : "Log", "resource_id" : id, "content" : content } },
      function success(data)       { $scope.log_detail.comments = $scope.log_detail.comments.concat(data.comment) },
      function error(data, status) { console.log(data) }
                )
  }
});
